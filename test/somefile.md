---
front_matter = true
what_else = 42

[foo]
bar = true
---

This is some example content! It has _markdown_ in it! **Wooo!**

Have a look at the front matter to find out how it works!
