# Heron

Not just _a_ static site generator -- _my_ static site generator

Note: a [Heron](https://en.wikipedia.org/wiki/Heron) is not a Pelican.

# General concepts

The main concept of Heron is a "collection". A collection is a directory that contains files. Collections are sorted by
their file name order.  
Files will be converted to HTML during generation; they have some _front matter_ that is TOML-parsed and
their main body that is converted according to its type. Each collection has an index file. The directory that you run
heron on is also a collection called "root". The root collection's index page is the page index.

Every file that starts with a `_` is not processed for content. Some of these files have special meanings:
* `_attributes.toml` defines attributes for the entire collection. It's the collection's "front matter".

Every folder that starts with a `_` is not processed for files. Some of these folders have special meanings:
* `_templates` contains all template files available to the generator
* `_raw` will be copied raw to the output. Use it for CSS and stuff.
* `_images` will be scanned and processed for images (see below for details)

All variables of the front matter of a thing are just passed into the template as is, along with `object` containing the
currently active object (collection or file). Some of these attributes have special meanings:
* `include_in_parent`: a special variable that signals that this directory is part of the parent directory and should
  not be treated as a collection itself. Allows you to organize your files more neatly.
* `template`: the name of the template to be loaded.
* `date`: if not filled out, the latest change date of the object
* `_filename`: the name of the file
* `_contents`: the rendered contents of the file
* `_url`: the (relative) URL to the object
* `_parent`: the parent collection of the object, None in case of the root collection

# Getting started

To create a page, you need templates and content. I'll assume you'll bring your own templates, but you can use the ones
in `test/_templates/` as a starting point.

Then run

    `heron.py help

And go from there.


## RSS feeds

You can get RSS feeds for your collections. Include these items in the collection attributes to make it work:

    [rss]
    title = "Title of your website"
    link = "https://link-to-your-website/"
    description = "your website's tagline"
    language = "en"

To pull one of your sub-collections as master-feed, add `root = True` into its attributes.

## Images

Image handling is a great and fun way to spend many hours!

The image loader will scan the `_images` special directory. Each image will be scanned placed as `_images` into each
template context. Each image object has functions you can call to generate the stuff you need. The simplest way to
include an image in your site is to call `[img:filename]`. This will read a template called `image.html`
from your templates and fill it with everything that's necessary. You can pass parameters to the rendering with a bit of
a weird format: simply do `param_name=whatever`; it'll parse until the next `param_name`, so no escaping or whatever is
necessary. All parameters are passed into the rendering context.
There are more functions if you need them, look them up in the `Image` class.

Once the content was generated, all necessary images are put into a folder called `_images` in your site. Only the files
that are necessary for your site are generated. If you want to include an image, you must use it somewhere.



# Unfinished features

* spaceless templates (see https://github.com/mitsuhiko/jinja2-htmlcompress)
* testing/coverage


# Why?

Look, there are a great number of static site generators out there. 
[I've been using one myself for a long time.](https://shezi.de/2016/01/08/welcome-to-jekyll.html)
But, as every programmer must write their own blog engine, this one is mine. I just wanted to do a few simple things
that turned out to be super-complicated with [Jekyll](https://jekyllrb.com/) or [Pelican](https://getpelican.com/) or
[Lektor](http://www.getlektor.com/) or [Wagtail](https://github.com/wagtail/wagtail-bakery) or 
[Hugo](https://gohugo.io/).
I just wanted to put images in a directory and have a simple way to show them. I just wanted to put some special markup
in my files that gets processed first. Stuff like that, you know!

So, I had to do it myself.


# License

Copyright © 2022 Johannes Spielmann

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
