#!/usr/bin/env python
import shutil
import time
from pathlib import Path

import commandeer
from livereload import Server

from heron import build_collections, images, files_cache
from heron.generate import generate
from heron.images import collect_images, generate_images
from heron.load_templates import load_templates
from heron.raw_files import copy_raw_files


def generate_command(directory_name: str, to="./_output/", include_drafts=False, persist_cache=True):
    """Generate the page from the data in `directory_name`. Output will be put into "_output".

    :param directory_name: The directory you want to generate from, you may pass "." here
    :param to: The target directory
    :param include_drafts: Should the output include draft files, ie those with `status = "draft"`?
    :param persist_cache: Should the generated content be stored for use next time, in a cache file?
    """

    start_time = time.time()

    dir_path = Path(directory_name)
    output_path = Path(to)
    files_cache.initialise_cache(dir_path)
    print(f"generating from {dir_path} to {output_path}")

    env = load_templates(dir_path)
    print("loaded templates")

    print("collecting images...")
    collect_images(dir_path / "_images", template_env=env)
    print(f"  found {images.count()} images")

    print("collecting content...")
    root_collection = build_collections.build(dir_path)
    print(f"  found {root_collection.count_files()} files in {root_collection.count_collections()} collections")

    print("processing files...")
    root_collection = build_collections.process_collection(root_collection, include_drafts=include_drafts)

    print("generating content...")
    shutil.rmtree(output_path, ignore_errors=True)
    generate(root_collection, env, output_path, output_path)

    print("generating images...")
    generate_images(output_path / "_images")

    print("copying assets...")
    copy_raw_files(dir_path / "_raw", output_path)

    if persist_cache:
        print("storing cache...")
        files_cache.save_cache(dir_path)

    end_time = time.time()
    print(f"processing took {end_time - start_time:.02f} s")


def server_command(directory_name: str, to=".", include_drafts=True):
    """Watch the input directory, regenerate when necessary and serve the site from the output directory.

    :param directory_name: The directory you want to generate from, you may pass "." here
    :param to: The target directory that will contain the generated site.
    :param include_drafts: Should the output include draft files, ie those with `status = "draft"`? NOTE THE DEFAULT!
    """

    dir_path = Path(directory_name)
    output_path = Path(to) / "_output"
    print(f"watching {dir_path}, generating to {output_path}")

    regen = lambda: generate_command(dir_path, output_path, include_drafts=include_drafts)
    regen()

    server = Server()
    server.watch(dir_path, regen)
    server.serve(root=output_path)


def test_command():

    test_dir = "/home/js/sync/obsidian/notes/shezi.de"

    # files_cache.scan_all(Path('../shezi'))
    generate_command(test_dir)
    # files_cache.initialise_cache(Path(test_dir))
    generate_command(test_dir)


if __name__ == "__main__":
    commandeer.cli()
