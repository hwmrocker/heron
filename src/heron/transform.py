import re

from markdown import markdown
from markupsafe import Markup as mark_safe

from . import images
from .parens import find_parens


def apply_sidenote_transform(contents):
    """Apply my own specific sidenote generation transformer."""

    sn_template = """<label for="mn-{title}" class="margin-toggle sidenote-number"></label><input type="checkbox" id="mn-{title}" class="margin-toggle"><span class="sidenote">{text}</span>"""

    def find_sidenotes(content):

        opening = "["
        closing = "]"

        parens = find_parens(content, opening=opening, closing=closing)

        sidenotes = []
        for paren in parens:
            inner = content[paren[0] + len(opening) : paren[1]]
            if inner.startswith("sn:"):
                title, text = inner.split(" ", 1)
                title = title[3:]
                text = text.strip()
                sidenotes.append((paren, title, text))

        return sidenotes

    def replace_sidenotes(content):

        sidenotes = find_sidenotes(content)
        for (s, e), title, text in reversed(sidenotes):
            sidenote_content = sn_template.format(title=title, text=text)
            content = content[:s] + sidenote_content + content[e + 1 :]

        return content

    new_contents = replace_sidenotes(contents)
    return mark_safe(new_contents)


def apply_image_transform(contents):
    """Replace images in the contents."""

    def md_less_p(s):
        if not s:
            return ""
        out = markdown(s)
        return out[3:-4]

    opening = "["
    closing = "]"

    image_places = find_parens(contents, opening=opening, closing=closing)
    requested = []

    for (s, e) in reversed(image_places):
        image_tag = contents[s + len(opening) : e]
        if not image_tag.startswith("img:"):
            continue
        if " " in image_tag:
            image_name, r_params = image_tag.split(" ", 1)
            image_name = image_name[4:].strip()

            split_indices = list(g.start() for g in re.finditer(r"( [a-zA-Z_]+=)", " " + r_params))
            if split_indices:
                splits = [r_params[split_indices[i - 1] : split_indices[i]] for i in range(1, len(split_indices))]
                splits.append(r_params[split_indices[-1] :])
            else:
                splits = []
            params_pretransformed = (s.strip().split("=", 1) for s in splits)
            params = dict((s[0], md_less_p(s[1])) for s in params_pretransformed)

        else:
            image_name = image_tag[4:].strip()
            params = {}

        requested.append((image_name, params))
        replacement = images.render(image_name, **params)

        contents = contents[:s] + replacement + contents[e + 1 :]

    return mark_safe(contents), requested


def transform(contents):

    contents = apply_sidenote_transform(contents)
    contents, requested_images = apply_image_transform(contents)
    transformed = markdown(contents, extensions=["fenced_code", "codehilite", "tables"])

    return transformed, requested_images
