"""
Scanning and caching of files.
"""
import os
import pickle
from pathlib import Path


class CachedFile:
    def __init__(self, name: str, directory: str, path: Path, mtime: float):
        self.name = name
        self.directory = directory
        self.path = path
        self.mtime = mtime
        self.c_build_content = None
        self.c_requested_images = None
        self.c_image_renditions = None

    def __str__(self):
        return f"CacheFile<{self.name}, {self.directory}, {self.path}, {self.mtime}>"

    def __repr__(self):
        return f"CacheFile<{self.name}, {self.directory}, {self.path}, {self.mtime}>"


file_cache: dict[Path, CachedFile] = {}
needs_save = False


def initialise_cache(root_dir: Path):
    """Build a cache of all available files."""

    global file_cache

    cache_path = root_dir / "_cache.pickle"
    if cache_path.exists():
        try:
            with open(cache_path, "rb") as f:
                file_cache = pickle.load(f)
        except (pickle.PickleError, EOFError):
            pass

    return file_cache


def save_cache(root_dir: Path):
    """Save the cache into a file for later use."""
    global needs_save

    if not needs_save:
        return
    cache_path = root_dir / "_cache.pickle"
    with open(cache_path, "wb") as f:
        pickle.dump(file_cache, f)

    needs_save = False


def get(key: Path) -> CachedFile:
    global needs_save

    entry = file_cache.get(key)
    current = CachedFile(key.name, str(key.parent), key, os.stat(key).st_mtime)
    if entry is None or current.mtime > entry.mtime:
        needs_save = True
        file_cache[key] = current
        return current
    return entry


def remove(key: Path) -> None:
    global needs_save

    if key in file_cache:
        needs_save = True
        del file_cache[key]

    return
