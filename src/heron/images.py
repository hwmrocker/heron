"""Image handling. Great fun!"""
import glob
import shutil
from io import BytesIO
from pathlib import Path
import PIL.Image

# module-level holder for found images
from jinja2 import Environment
from markupsafe import Markup
from tqdm import tqdm

from heron import files_cache

_images: dict[str, "Image | SVGimage"] = {}

# module_level holder for template environment
_templates: Environment = None


DESTINATION_PREFIX = "/_images"


class Image:
    def __init__(self, absolute_path: Path, filename: str, alt="", desc=""):
        self.absolute_path = absolute_path
        self.filename = filename
        self.alt = alt
        self.desc = desc

        p = Path(self.filename)
        self._basename = str(p.parent / p.stem)
        self._ending = p.suffix

        self._width = None

        self._to_generate = {}

    def render(self, alt="", desc="", **kwargs) -> str:
        """Read the `image.html` template and fill it with this image."""
        self.alt = alt
        self.desc = desc

        template = _templates.get_template("image.html")
        contents = template.render(image=self, **kwargs)

        return Markup(contents)

    def srcset(self):
        """Create a srcset for this image, with power-of-two size reductions until 320w."""
        width = self.get_width()
        parts = []
        url = self.create_one(width)
        parts.append(f"{url} {width}w")
        while width > 320:
            width = int(width / 2)
            url = self.create_one(width)
            parts.append(f"{url} {width}w")

        return ",".join(parts)

    def src_small(self):
        """Create the smallest rendition and return the URL."""
        width = self.get_width()
        while width > 320:
            width = int(width / 2)
        return self.create_one(width)

    def create_one(self, width):
        """Create a single rendition with given `width`."""
        if width in self._to_generate:
            return f"{DESTINATION_PREFIX}/{self._to_generate[width]}"
        self._to_generate[width] = f"{self._basename}-{width}{self._ending}"
        return f"{DESTINATION_PREFIX}/{self._to_generate[width]}"

    def get_width(self):
        if self._width:
            return self._width
        self._pil = PIL.Image.open(self.absolute_path)
        self._width = self._pil.width
        return self._width

    def generate(self, output_path: Path):
        """Put all the files into the output directory."""

        if not self._to_generate:
            return

        # make necessary directories
        base_path = output_path / Path(self.filename).parent
        base_path.mkdir(parents=True, exist_ok=True)

        c_file = files_cache.get(self.absolute_path)
        if c_file.c_image_renditions:
            for path, image_bytes in c_file.c_image_renditions:
                with open(output_path / path, "wb") as f:
                    f.write(image_bytes)
            return

        size = self._pil.size

        variant_cache = []
        # create files with widths
        for width, rel_path in tqdm(self._to_generate.items()):
            # print(width, output_path / rel_path)
            ratio = width / size[0]
            new_size = (int(ratio * size[0]), int(ratio * size[1]))
            # print("  ", ratio, ratio * size[0], ratio * size[1])
            variant = self._pil.resize(new_size)
            buf = BytesIO()
            variant.save(buf, format=self._pil.format)
            image_bytes = buf.getvalue()
            variant_cache.append((rel_path, image_bytes))
            with open(output_path / rel_path, "wb") as f:
                f.write(image_bytes)
        c_file.c_image_renditions = variant_cache

    def __str__(self):
        return f"Image<{self.filename}>"


class SVGImage:
    def __init__(self, absolute_path: Path, filename: str, alt="", desc=""):
        self.absolute_path = absolute_path
        self.filename = filename
        self.alt = alt
        self.desc = desc

        p = Path(self.filename)
        self._basename = str(p.parent / p.stem)
        self._ending = p.suffix
        self._create = False

    def render(self, alt="", desc="", **kwargs) -> str:
        """Read the `image.html` template and fill it with this image."""
        self.alt = alt
        self.desc = desc
        self._create = True

        template = _templates.get_template("svg.html")
        contents = template.render(image=self, **kwargs)

        return Markup(contents)

    def src(self):
        self._create = True
        return f"{DESTINATION_PREFIX}/{self._basename}.svg"

    def generate(self, output_path: Path):

        if not self._create:
            return

        # make necessary directories
        base_path = output_path / Path(self.filename).parent
        base_path.mkdir(parents=True, exist_ok=True)

        shutil.copy(
            self.absolute_path,
            base_path / self.absolute_path.name
        )


def collect_images(image_path: Path, template_env):

    global _templates
    _templates = template_env

    file_paths = glob.glob("**/*.jpg", root_dir=image_path)

    for file_path in file_paths:
        im = Image(image_path / file_path, file_path)
        _images[file_path] = im

    svg_file_paths = glob.glob("**/*.svg", root_dir=image_path)

    for svg_file_path in svg_file_paths:
        im = SVGImage(image_path / svg_file_path, svg_file_path)
        _images[svg_file_path] = im

    return _images


def generate_images(output_path: Path):

    for image in tqdm(_images.values()):
        image.generate(output_path)


def render(image_name, **kwargs):
    """Render a single image from our collection."""
    img = get(image_name)
    if not img:
        return f"[NOT FOUND: {repr(image_name)}]"

    output = img.render(**kwargs)

    return output


def get(image_name) -> Image:
    """Find a single image in our collection."""
    img = _images.get(image_name)
    if not img:
        print(f"  could not find image: {repr(image_name)}")
    return img


def count():
    return len(_images)
