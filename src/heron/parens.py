"""
Give me a string and I'll extract all the (nested) parentheses out of it.
"""

# Author: Johannes Spielmann <j@spielmannsolutions.com>
# License: MIT License
#
# Copyright 2021, 2022 Johannes Spielmann
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


def find_parens(s, opening="(", closing=")"):
    """
    Find all opening/closing parenthesis pairs.

    Ignores mismatched parentheses. Returns from innermost to outermost, left to right.
    """

    res = []
    opened = []

    for index in range(len(s)):
        if s[index:].startswith(opening):
            opened.append(index)
        elif s[index:].startswith(closing):
            if opened:
                open_index = opened.pop()
                res.append((open_index, index))

    return res


def extract_parens(s, opening="(", closing=")"):
    parens = find_parens(s, opening=opening, closing=closing)
    res = [s[p[0] : p[1] + len(closing)] for p in parens]

    return res


def extract_contents(s, opening="(", closing=")"):
    parens = find_parens(s, opening=opening, closing=closing)
    res = [s[p[0] + len(opening) : p[1]] for p in parens]

    return res


# noinspection Assert
def run_tests():
    def show_single(s, opening="(", closing=")"):
        print(s)
        print(" ", find_parens(s, opening=opening, closing=closing))
        print(" ", extract_parens(s, opening=opening, closing=closing))
        print(" ", extract_contents(s, opening=opening, closing=closing))

    s = "(1) (2) (3)"
    show_single(s)
    assert extract_contents(s) == ["1", "2", "3"]

    s = "( (1) )"
    show_single(s)
    assert extract_contents(s) == [
        "1",
        " (1) ",
    ]

    s = "( (1, 2), (3, 4), (5, 6) )"
    show_single(s)
    assert extract_contents(s) == ["1, 2", "3, 4", "5, 6", " (1, 2), (3, 4), (5, 6) "]

    s = "(((((x)))))"
    show_single(s)
    assert extract_contents(s) == ["x", "(x)", "((x))", "(((x)))", "((((x))))"]

    s = "(((((x)"
    show_single(s)
    assert extract_contents(s) == ["x"]

    s = "(x))))"
    show_single(s)
    assert extract_contents(s) == ["x"]

    s = "[img:whatever foo=bla]"
    show_single(s, opening="[img:", closing="]")
    assert extract_contents(s, opening="[img:", closing="]") == ["whatever foo=bla"]

    s = "[img:whatever foo=bla:img]"
    show_single(s, opening="[img:", closing=":img]")
    assert extract_contents(s, opening="[img:", closing=":img]") == ["whatever foo=bla"]


if __name__ == "__main__":
    run_tests()
