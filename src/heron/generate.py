"""
Take a collection and generate its contents into HTML.
"""
from datetime import datetime
from pathlib import Path
from typing import Dict

import feedgenerator
import pytz

from . import images
from .build_collections import Collection


def generate(collection: Collection, template_env, output_path: Path, root_path: Path):

    output_path.mkdir(parents=True, exist_ok=True)

    generate_collection_index(collection, template_env, output_path)

    if "rss" in collection.attributes:
        generate_collection_feed(collection, output_path, root_path)

    for file in collection:
        generate_file(file, template_env, output_path)

    for name, sub_collection in collection.collections.items():
        generate(sub_collection, template_env, output_path / name, root_path)


def generate_collection_index(collection: Collection, template_env, output_path: Path):

    template_name = collection.attributes.get("template", "collection.html")
    template = template_env.get_template(template_name)

    contents = template.render(object=collection, _images=images, **collection.attributes)

    output_file_path = output_path / "index.html"
    output_file_path.write_text(contents)


def generate_collection_feed(collection: Collection, output_path: Path, root_path: Path):

    rss_attribs = collection.attributes["rss"]
    link = rss_attribs.get("link", "")
    base_url = rss_attribs.get("base_url", link)
    combinable_datetime = datetime.min.time()
    timezone = pytz.timezone("Europe/Berlin")

    feed = feedgenerator.Rss201rev2Feed(
        title=rss_attribs.get("title"),
        link=link,
        description=rss_attribs.get("description"),
        language=rss_attribs.get("language"),
    )
    items = []
    for file in collection.files:
        feed_date: datetime = (
            file["date"] if isinstance(file["date"], datetime) else datetime.combine(file["date"], combinable_datetime)
        )
        if feed_date.tzinfo is None:
            feed_date = timezone.localize(feed_date)
        items.append(
            {
                "title": file.get("name") or file.get("title") or file["_basename"],
                "link": base_url + file["_url"],
                "description": file["_contents"],
                "pubdate": feed_date,
            }
        )
    items.sort(key=lambda item: item["pubdate"], reverse=True)
    for item in items:
        feed.add_item(**item)

    feed_contents = feed.writeString(encoding="utf-8")

    if rss_attribs.get("root"):
        output_file_path = root_path / "feed.xml"
    else:
        output_file_path = output_path / "feed.xml"
    output_file_path.write_text(feed_contents)


def generate_file(file: Dict, template_env, output_path: Path):

    template_name = file.get("template", "post.html")
    if not template_name.endswith(".html"):
        template_name = template_name + ".html"
    template = template_env.get_template(template_name)

    contents = template.render(object=file, _parent=file["_parent"], _images=images)

    output_file_path = output_path / (file["_basename"] + ".html")
    output_file_path.write_text(contents)
