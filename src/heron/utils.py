from pathlib import Path


def get_file_paths(path: Path):

    return list(sorted([p for p in path.iterdir() if p.is_file() and not p.name.startswith("_")]))


def get_dir_paths(path: Path):
    return list(sorted([p for p in path.iterdir() if p.is_dir() and not p.name.startswith("_")]))
